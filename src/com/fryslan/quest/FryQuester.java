package com.fryslan.quest;

import com.fryslan.quest.actions.QuestAction;
import com.fryslan.quest.actions.cooksassistant.CooksAssistant;
import com.fryslan.quest.actions.cooksassistant.CooksAssistantActions;
import com.fryslan.quest.actions.sheepshearer.SheepShearer;
import org.powerbot.script.*;
import org.powerbot.script.rt4.ClientContext;

import java.awt.*;
import java.util.ArrayList;

@Script.Manifest(name = "FryQuester", description = "Completes Quests for you!")

public class FryQuester extends PollingScript<ClientContext> implements PaintListener, MessageListener {

    ArrayList<QuestAction> actions = new ArrayList<>();

    @Override
    public void poll() {
        for (QuestAction action : actions) {
            if (!action.completed()) {
                System.out.println(action.Status());
                action.run();
            }
        }

    }

    @Override
    public void start() {
        actions.add(new CooksAssistant(ctx));
        actions.add(new SheepShearer(ctx));
    }

    @Override
    public void stop() {
        super.stop();
    }

    @Override
    public void repaint(Graphics graphics) {

    }

    @Override
    public void messaged(MessageEvent messageEvent) {
        System.out.println(messageEvent.type() + " - " + messageEvent.text());
        if (messageEvent.type() == 0) {
            if (messageEvent.text().contentEquals("You put the grain in the hopper. You should now pull the lever nearby to operate the hopper.")) {
                CooksAssistantActions.grainInHopper = true;
            }
        }
    }

}
