package com.fryslan.quest.actions.traverse;

import org.powerbot.script.Condition;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.GameObject;

public class SmartWalkingObject {

    private final ClientContext ctx;
    private final GameObject object;
    private final String option;
    private final Tile before;
    private final Tile after;

    public SmartWalkingObject(ClientContext ctx, GameObject object, String option, Tile before, Tile after) {
        this.ctx = ctx;
        this.object = object;
        this.option = option;
        this.before = before;
        this.after = after;
    }

    public boolean near() {
        return before.distance() < 15;
    }

    public boolean complete() {
        if (after.distance() < 15 && after.reachable()) {
            return true;
        } else {
            if (object.valid()) {
                if (object.inViewport()) {
                    object.interact(option, object.name());
                    Condition.wait(() -> after.reachable(), 50, 30);
                    return after.reachable();
                } else {
                    ctx.camera.turnTo(object);
                    if (!object.inViewport()) {
                        if (ctx.movement.walkTo(object)) {
                            Condition.wait(() -> object.inViewport(), 50, 30);
                        }
                    }
                }
            } else {
                ctx.movement.walkTo(before);
            }
        }
        return false;
    }
}
