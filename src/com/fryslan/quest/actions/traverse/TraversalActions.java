package com.fryslan.quest.actions.traverse;

import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.TilePath;

public class TraversalActions {

    public static void smartWalk(ClientContext ctx, TilePath path, SmartWalkingObject smartWalkingObject) {

        if (smartWalkingObject.near()) {
            if (smartWalkingObject.complete()) {
                path.traverse();
            }
        } else {
            path.traverse();
        }
    }
}
