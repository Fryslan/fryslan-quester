package com.fryslan.quest.actions.sheepshearer;

import com.fryslan.quest.actions.QuestAction;
import com.fryslan.quest.data.Quest;
import org.powerbot.script.rt4.ClientContext;

public class SheepShearer implements QuestAction {

    private final ClientContext ctx;
    private String status;

    public SheepShearer(ClientContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public void run() {
        switch (ctx.varpbits.varpbit(quest().getSetting(), quest().getMask())) {
            case 0:
                status = "Starting Quest";
                SheepShearerActions.startQuest(ctx);
                break;
            case 1:
                status = "Gathering the wool, spin the wool and give the balls to Fred.";
                SheepShearerActions.getProducts(ctx);
                break;
        }
    }

    @Override
    public boolean completed() {
        return ctx.varpbits.varpbit(quest().getSetting(), quest().getMask()) == quest().getCompletedValue();
    }

    @Override
    public Quest quest() {
        return Quest.SHEEP_SHEARER;
    }

    @Override
    public String Status() {
        return String.format(
                "Quest: %s[%b]\n" +
                        "Progress: %d [%d -> %s >> %d]\n" +
                        "Status: %s",
                quest().name(),
                completed(),
                ctx.varpbits.varpbit(quest().getSetting(), quest().getMask()),
                ctx.varpbits.varpbit(quest().getSetting()),
                Integer.toBinaryString(quest().getMask()),
                quest().getShift(),
                status
        );
    }
}
