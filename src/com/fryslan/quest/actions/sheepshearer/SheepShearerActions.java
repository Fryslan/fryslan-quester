package com.fryslan.quest.actions.sheepshearer;

import com.fryslan.quest.actions.QuestConversationActions;
import com.fryslan.quest.actions.QuestItemActions;
import com.fryslan.quest.actions.QuestNpcActions;
import org.powerbot.script.Condition;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.*;

public class SheepShearerActions {
    private static final String[] CONVERSATIONS = {"I'm looking for a quest.", "Yes."};
    private static final Tile FREDS_HOUSE = new Tile(3189, 3272, 0);
    private static final Tile SHEEP_PEN = new Tile(3200, 3266, 0);
    private static final Tile WHEEL_ROOM = new Tile(3209, 3214, 1);
    private static boolean spinning;

    public static void startQuest(ClientContext ctx) {
        Npc fred = ctx.npcs.toStream().name("Fred the farmer").nearest().first();
        if (QuestConversationActions.chatOpen(ctx)) {
            QuestConversationActions.printChatOptions(ctx);
            QuestConversationActions.simpleConversation(ctx, CONVERSATIONS);
        } else {
            QuestNpcActions.walkToAndInteract(ctx, fred, "Talk-to", FREDS_HOUSE, () -> QuestConversationActions.chatOpen(ctx));
        }
    }

    public static void getProducts(ClientContext ctx) {
        Item shears = ctx.inventory.toStream().name("Shears").first();
        GroundItem gShears = ctx.groundItems.toStream().name("Shears").first();
        Npc sheep = ctx.npcs.toStream().id(2693, 2694, 2695, 2786, 2787).action("Shear").nearest().first();
        GameObject wheel = ctx.objects.toStream().name("Spinning wheel").nearest().first();
        long woolCount = ctx.inventory.toStream().name("Wool").count();
        long ballCount = ctx.inventory.toStream().name("Ball of wool").count();
        Component ballWidget = ctx.widgets.component(270, 14, 29);

        if (shears.valid()) {
            if (ballCount == 20) {
                startQuest(ctx);
            } else if (woolCount < 20 && ballCount == 0) {
                shearSheep(ctx, sheep, woolCount);
            } else {
                if (ballCount < 20) {
                    spinBalls(ctx, wheel, ballWidget);
                }
            }
        } else {
            QuestItemActions.walkToAndPickup(ctx, gShears, shears, FREDS_HOUSE);
        }
    }

    private static void spinBalls(ClientContext ctx, GameObject wheel, Component ballWidget) {
        if (WHEEL_ROOM.distance() < 10 && WHEEL_ROOM.reachable()) {
            System.out.println(spinning);
            if (spinning) {
                Condition.wait(() -> ctx.players.local().animation() != -1 || ctx.inventory.toStream().name("Ball of wool").count() == 0, 50, 30);
                if (ctx.players.local().animation() == -1 || ctx.inventory.toStream().name("Ball of wool").count() == 0) {
                    spinning = false;
                }
            }
            if (ctx.players.local().animation() == -1) {

                if (ballWidget.valid() && ballWidget.visible() && ballWidget.width() > 0) {
                    ballWidget.click();
                    Condition.wait(() -> ctx.players.local().animation() != -1, 50, 30);
                    spinning = ctx.players.local().animation() != -1;
                    System.out.println("Spinning");
                } else {
                    if (wheel.inViewport()) {
                        wheel.interact("Spin", wheel.name());
                        Condition.wait(() -> ballWidget.valid() && ballWidget.visible() && ballWidget.width() > 0, 50, 30);
                    } else {
                        ctx.camera.turnTo(wheel);
                        if (!wheel.inViewport()) {
                            if (ctx.movement.step(wheel)) {
                                Condition.wait(() -> wheel.inViewport(), 50, 30);
                            }
                        }
                    }
                }
            }
        } else {
            ctx.movement.walkTo(WHEEL_ROOM);
        }
    }

    private static void shearSheep(ClientContext ctx, Npc sheep, long woolCount) {
        if (SHEEP_PEN.distance() < 15 && SHEEP_PEN.reachable()) {
            QuestNpcActions.walkToAndInteract(ctx, sheep, "Shear", sheep.tile(), () -> woolCount != ctx.inventory.toStream().name("Wool").count());
        } else {
            ctx.movement.walkTo(SHEEP_PEN);
        }
    }
}
