package com.fryslan.quest.actions.cooksassistant;

import com.fryslan.quest.actions.QuestConversationActions;
import com.fryslan.quest.actions.QuestItemActions;
import com.fryslan.quest.actions.QuestNpcActions;
import com.fryslan.quest.actions.QuestObjectActions;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.*;

public class CooksAssistantActions {
    private static final String[] CONVERSATIONS = {"What's wrong?", "Yes."};
    private static final Tile MILL_FLOOR_0 = new Tile(3167, 3305, 0);
    private static final Tile MILL_FLOOR_2 = new Tile(3164, 3306, 2);
    private static final Tile KITCHEN = new Tile(3210, 3215, 0);
    private static final Tile BASEMENT = new Tile(3216, 9623, 0);
    private static final Tile COW_PEN = new Tile(3173, 3317, 0);
    private static final Tile CHICKEN_PEN = new Tile(3181, 3298, 0);
    private static final Tile WHEAT_FIELD = new Tile(3162, 3292, 0);
    public static boolean grainInHopper = false;
    private static boolean hasEgg;
    private static boolean hasMilk;
    private static boolean hasFlour;

    public static void startQuest(ClientContext ctx) {
        if (QuestConversationActions.chatOpen(ctx)) {
            QuestConversationActions.printChatOptions(ctx);
            QuestConversationActions.simpleConversation(ctx, CONVERSATIONS);
        } else {
            talkToCook(ctx);
        }

    }

    private static void talkToCook(ClientContext ctx) {
        Npc cook = ctx.npcs.toStream().name("Cook").nearest().first();
        if (cook.valid()) {
            QuestConversationActions.talkTo(ctx, cook);
        } else {
            ctx.movement.walkTo(KITCHEN, false);
        }
    }

    public static void getIngredients(ClientContext ctx) {

        Item egg = ctx.inventory.toStream().name("Egg").first();
        GroundItem gEgg = ctx.groundItems.toStream().name("Egg").nearest().first();
        Item flour = ctx.inventory.toStream().name("Pot of flour").first();
        Item milk = ctx.inventory.toStream().name("Bucket of milk").first();
        Item bucket = ctx.inventory.toStream().name("Bucket").first();
        GroundItem gBucket = ctx.groundItems.toStream().name("Bucket").nearest().first();
        Item pot = ctx.inventory.toStream().name("Pot").first();
        GroundItem gPot = ctx.groundItems.toStream().name("Pot").nearest().first();
        Item grain = ctx.inventory.toStream().name("Grain").first();
        GameObject oWheat = ctx.objects.toStream().name("Wheat").nearest().first();
        GameObject hopper = ctx.objects.toStream().name("Hopper").nearest().first();
        GameObject lever = ctx.objects.toStream().name("Hopper controls").nearest().first();
        GameObject bin = ctx.objects.toStream().name("Flour bin").nearest().first();

        if (hasMilk && hasEgg && hasFlour) {
            if (!QuestConversationActions.chatOpen(ctx)) {
                talkToCook(ctx);
            } else {
                QuestConversationActions.simpleConversation(ctx);
            }
        } else {
            if (!pot.valid() && !flour.valid()) {
                QuestItemActions.walkToAndPickup(ctx, gPot, pot, KITCHEN);
            } else if (!bucket.valid() && !milk.valid()) {
                QuestItemActions.walkToAndPickup(ctx, gBucket, bucket, BASEMENT);
            } else if (!milk.valid()) {
                getMilk(ctx, milk);
            } else if (!egg.valid()) {
                QuestItemActions.walkToAndPickup(ctx, gEgg, egg, CHICKEN_PEN);
            } else if (!flour.valid()) {
                getFlour(ctx, flour, grain, oWheat, hopper, lever, bin);
            } else {
                hasEgg = egg.valid();
                hasFlour = flour.valid();
                hasMilk = milk.valid();
            }

        }

    }

    private static void getFlour(ClientContext ctx, Item flour, Item grain, GameObject oWheat, GameObject hopper, GameObject lever, GameObject bin) {

        if (!grainInHopper) {
            if (grain.valid()) {
                QuestObjectActions.walkToAndUseItemOn(ctx, hopper, grain, "Grain -> Hopper", MILL_FLOOR_2, () -> !grain.valid());
            } else {
                QuestObjectActions.walkToAndInteract(ctx, oWheat, "Pick", WHEAT_FIELD, () -> grain.valid());
            }
        } else {
            boolean processed = ctx.varpbits.varpbit(203, 1) == 1;
            if (processed) {
                QuestObjectActions.walkToAndInteract(ctx, bin, "Empty", MILL_FLOOR_0, () -> flour.valid());
            } else {
                QuestObjectActions.walkToAndInteract(ctx, lever, "Operate", MILL_FLOOR_2, () -> !grain.valid());
            }
        }
    }

    private static void getMilk(ClientContext ctx, Item milk) {
        Npc cow = ctx.npcs.toStream().id(1172).nearest().first();

        if (ctx.players.local().animation() == -1) {
            if (cow.valid()) {
                QuestNpcActions.walkToAndInteract(ctx, cow, "Milk", COW_PEN, () -> milk.valid());
            } else {
                ctx.movement.walkTo(COW_PEN, false);
            }
        }

    }
}
