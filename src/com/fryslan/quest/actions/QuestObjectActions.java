package com.fryslan.quest.actions;

import org.powerbot.script.Condition;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.Game;
import org.powerbot.script.rt4.GameObject;
import org.powerbot.script.rt4.Item;

import java.util.concurrent.Callable;

public class QuestObjectActions {

    public static void walkToAndUseItemOn(ClientContext ctx, GameObject object, Item item, String action, Tile location, Callable<Boolean> condition) {
        if (location.distance() < 15 && location.reachable()) {
            if (object.valid()) {
                if (object.inViewport()) {
                    itemOnObject(ctx, item, object, action);
                    Condition.wait(condition, 50, 30);
                } else {
                    ctx.camera.turnTo(object);
                    if (!object.inViewport()) {
                        if (ctx.movement.step(location)) {
                            Condition.wait(() -> object.inViewport(), 50, 30);
                        }
                    }
                }
            } else {
                Condition.wait(() -> object.valid(), 50, 30);
            }
        } else {
            ctx.movement.walkTo(location);
        }
    }

    public static void walkToAndInteract(ClientContext ctx, GameObject object, String action, Tile location, Callable<Boolean> condition) {
        if (location.distance() < 15 && location.reachable()) {
            if (object.valid()) {
                if (object.inViewport()) {
                    if (object.interact(action, object.name())) {
                        Condition.wait(condition);
                    }
                } else {
                    ctx.camera.turnTo(object);
                    if (!object.inViewport()) {
                        if (ctx.movement.step(location)) {
                            Condition.wait(() -> object.inViewport(), 50, 30);
                        }
                    }
                }
            } else {
                Condition.wait(() -> object.valid(), 50, 30);
            }
        } else {
            ctx.movement.walkTo(location);
        }
    }

    public static void itemOnObject(ClientContext ctx, Item item, GameObject object, String action) {
        if (ctx.game.tab(Game.Tab.INVENTORY)) {
            if (!ctx.inventory.selectedItem().valid()) {
                Condition.wait(() -> !item.interact("Use", item.name()) || ctx.inventory.selectedItem().valid(), 50, 40);
            } else {
                object.click(3);
                if (ctx.menu.opened()) {
                    ctx.menu.click(menuCommand -> menuCommand.option.contentEquals(action));
                }
            }
        }
    }
}
