package com.fryslan.quest.actions;

import org.powerbot.script.Condition;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.Npc;

import java.util.concurrent.Callable;

public class QuestNpcActions {

    public static void walkToAndInteract(ClientContext ctx, Npc npc, String action, Tile location, Callable<Boolean> condition) {
        if (npc.valid() && npc.tile().reachable()) {
            if (npc.inViewport()) {
                if (npc.interact(action, npc.name())) {
                    Condition.wait(condition, 50, 30);
                }
            } else {
                ctx.camera.turnTo(npc);
                if (!npc.inViewport()) {
                    if (ctx.movement.step(npc)) {
                        Condition.wait(() -> npc.inViewport(), 50, 30);
                    }
                }
            }
        } else {
            ctx.movement.walkTo(location);
        }
    }
}
