package com.fryslan.quest.actions;

import org.powerbot.script.Condition;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.GroundItem;
import org.powerbot.script.rt4.Item;

public class QuestItemActions {
    public static void walkToAndPickup(ClientContext ctx, GroundItem gItem, Item item, Tile location) {
        if (gItem.valid()) {
            pickupItem(ctx, gItem, item);
        } else {
            walkToItem(ctx, gItem, location);
        }
    }

    private static void pickupItem(ClientContext ctx, GroundItem gItem, Item item) {
        if (gItem.inViewport() && gItem.tile().reachable()) {
            if (gItem.interact("Take", gItem.name())) {
                long count = ctx.inventory.toStream().name(item.name()).count();
                Condition.wait(() -> count != ctx.inventory.toStream().name(item.name()).count(), 50, 30);
            }
        } else {
            ctx.camera.turnTo(gItem);
            if (!item.inViewport()) {
                if (ctx.movement.step(gItem)) {
                    Condition.wait(() -> gItem.inViewport(), 50, 30);
                }
            }

        }
    }

    private static void walkToItem(ClientContext ctx, GroundItem item, Tile location) {
        if (location.distance() > 10) {
            ctx.movement.walkTo(location);
        } else {
            Condition.wait(() -> item.valid(), 50, 30);
        }
    }
}
