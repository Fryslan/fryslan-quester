package com.fryslan.quest.actions;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.Component;
import org.powerbot.script.rt4.Npc;

public class QuestConversationActions {
    private static final int[] CHAT_WIDGETS = {231, 219, 217};

    public static void printChatOptions(ClientContext ctx) {
        for (Component s : ctx.chat.optionBarComponent().components()) {
            if (s.valid() && s.text() != null) {
                System.out.println(s.text());
            }
        }
    }

    public static void clickChatOption(ClientContext ctx, String[] options) {
        for (String option : options) {
            for (Component s : ctx.chat.optionBarComponent().components()) {
                if (s.valid() && s.text() != null && s.text().contentEquals(option)) {
                    if (s.click()) {
                        Condition.wait(() -> s.valid(), 50, 30);
                    }
                }
            }
        }
    }

    public static boolean chatOpen(ClientContext ctx) {
        for (int id : CHAT_WIDGETS) {
            Component chatComponent = ctx.widgets.component(id, 0);
            if (chatComponent.valid() && chatComponent.visible() && chatComponent.width() > 0) {
                return true;
            }
        }
        return false;
    }

    public static void talkTo(ClientContext ctx, Npc npc) {
        if (npc.tile().reachable()) {
            if (npc.inViewport()) {
                if (npc.interact("Talk-to", npc.name())) {
                    Condition.wait(() -> ctx.chat.chatting(), 50, 30);
                }
            } else {
                if (npc.tile().distance() < 6) {
                    ctx.camera.turnTo(npc);
                    Condition.wait(() -> npc.inViewport(), 50, 30);
                } else {
                    if (ctx.movement.step(npc.tile())) {
                        Condition.wait(() -> npc.inViewport(), 50, 30);
                    }
                }
            }
        } else {
            if (ctx.movement.walkTo(npc.tile())) {
                Condition.wait(() -> npc.tile().reachable(), 50, 30);
            }
        }
    }

    public static void simpleConversation(ClientContext ctx, String[] options) {
        if (ctx.chat.canContinue()) {
            if (ctx.chat.clickContinue(true)) {
                String chat = ctx.chat.chatComponent().text();
                Condition.wait(() -> chat != ctx.chat.chatComponent().text(), 50, 30);
            }
        } else {
            clickChatOption(ctx, options);
        }
    }

    public static void simpleConversation(ClientContext ctx) {
        if (ctx.chat.canContinue()) {
            if (ctx.chat.clickContinue(true)) {
                String chat = ctx.chat.chatComponent().text();
                Condition.wait(() -> chat != ctx.chat.chatComponent().text(), 50, 30);
            }
        }
    }
}
