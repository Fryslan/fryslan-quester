package com.fryslan.quest.actions;

import com.fryslan.quest.data.Quest;

public interface QuestAction {
    void run();

    boolean completed();

    Quest quest();

    String Status();
}
