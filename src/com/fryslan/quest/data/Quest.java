package com.fryslan.quest.data;

public enum Quest {
    COOKS_ASSISTANT(29, 3, 0, 2),
    SHEEP_SHEARER(179, 31, 0, 21);

    private final int setting;
    private final int mask;
    private final int shift;
    private final int completedValue;

    Quest(int setting, int mask, int shift, int completedValue) {
        this.setting = setting;
        this.mask = mask;
        this.shift = shift;
        this.completedValue = completedValue;
    }

    public int getSetting() {
        return setting;
    }

    public int getMask() {
        return mask;
    }

    public int getShift() {
        return shift;
    }

    public int getCompletedValue() {
        return completedValue;
    }
}
